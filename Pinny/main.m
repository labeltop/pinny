//
//  main.m
//  Pinny
//
//  Created by hasan on 6/3/13.
//  Copyright (c) 2013 Hasan Adil. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
