//
//  ViewController.m
//  Pinny
//
//  Created by hasan on 6/3/13.
//  Copyright (c) 2013 Hasan Adil. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "ViewController.h"
#import "PSCollectionView.h"
#import "PinnyCollectionCell.h"
#import "AFJSONRequestOperation.h"

#define PORTRAIT_COLS 3
#define LANDSCAPE_COLS 4

@interface ViewController () <UIScrollViewDelegate, PSCollectionViewDelegate, PSCollectionViewDataSource, UIImagePickerControllerDelegate, CLLocationManagerDelegate>

@property (nonatomic, strong) PSCollectionView* collectionView;

//Api specs are at: https://developer.forecast.io/docs/v2#forecast_call
@property (nonatomic, strong) NSDictionary* forecast;
@property (nonatomic, strong) NSArray* dailyForecasts;

//location services
@property (nonatomic, strong) CLLocationManager *locationManager;

@end

@implementation ViewController

@synthesize collectionView, forecast, dailyForecasts, locationManager;

- (void)viewDidLoad
{
    NSLog(@"%s", __FUNCTION__);
    [super viewDidLoad];
    [[self view] setBackgroundColor:[UIColor scrollViewTexturedBackgroundColor]];
    
    self.collectionView = [[PSCollectionView alloc] initWithFrame:self.view.bounds];
    self.collectionView.delegate = self; // This is for UIScrollViewDelegate
    self.collectionView.collectionViewDelegate = self;
    self.collectionView.collectionViewDataSource = self;
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.autoresizingMask = ~UIViewAutoresizingNone;
    self.collectionView.numColsPortrait = PORTRAIT_COLS;
    self.collectionView.numColsLandscape = LANDSCAPE_COLS;    
    [[self view] addSubview:self.collectionView];
}

-(void) viewDidAppear:(BOOL)animated
{
    NSLog(@"%s", __FUNCTION__);
    [super viewDidAppear:animated];
    [self fetchLocation];
}

-(void) tapRefresh:(id)sender
{
    NSLog(@"%s", __FUNCTION__);
    [self fetchLocation];
}

-(void) fetchLocation
{
    NSLog(@"%s", __FUNCTION__);
    UIActivityIndicatorView* activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    UIBarButtonItem* btnActivity = [[UIBarButtonItem alloc] initWithCustomView:activity];
    [[self navigationItem] setRightBarButtonItem:btnActivity animated:YES];
    [activity startAnimating];    
    [[self navigationItem] setTitle:@"Loading..."];
    
    if (![self locationManager]) {
        [self setLocationManager:[[CLLocationManager alloc] init]];
        [[self locationManager] setDelegate:self];
        [[self locationManager] setDesiredAccuracy:kCLLocationAccuracyBest];
        [[self locationManager] setDistanceFilter:kCLDistanceFilterNone];        
    }
    [[self locationManager] startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"%s", __FUNCTION__);
    [manager stopUpdatingLocation];
    
    CLLocationCoordinate2D coordinate = [newLocation coordinate];
    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    
    [[self navigationItem] setTitle:[NSString stringWithFormat:@"%@, %@", latitude, longitude]];
    
    [self fetchForecastAtLatitude:latitude andLongitude:longitude];
}

-(void) fetchForecastAtLatitude:(NSString*)lat andLongitude:(NSString*)lon {
    NSString* key = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"forecast_io_key"];
    NSString* address = [NSString stringWithFormat:@"https://api.forecast.io/forecast/%@/%@,%@", key, lat, lon];
    NSLog(@"%s address: %@", __FUNCTION__, address);
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:address]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                             self.forecast = (NSDictionary *) JSON;
                                             [self reloadForecast];                                                                                            
                                          } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                              NSLog(@"Request Failure Because %@",[error userInfo]);
                                              
                                              [self stop];
                                              UIAlertView* errorAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil];
                                              [errorAlert show];
                                          }
                                        ];
    [operation start];
}

-(void) reloadForecast
{
    NSLog(@"%s", __FUNCTION__);
    NSDictionary* daily = [self.forecast objectForKey:@"daily"];
    self.dailyForecasts = [daily objectForKey:@"data"];
    NSLog(@"%s %@", __FUNCTION__, dailyForecasts);
    [[self collectionView] reloadData];
    [self stop];
}

-(void) stop
{
    UIBarButtonItem* btnRefresh = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(tapRefresh:)];
    [[self navigationItem] setRightBarButtonItem:btnRefresh animated:YES];
}

- (Class)collectionView:(PSCollectionView *)collectionView cellClassForRowAtIndex:(NSInteger)index {
    NSLog(@"%s", __FUNCTION__);
    return [PinnyCollectionCell class];
}

- (NSInteger)numberOfRowsInCollectionView:(PSCollectionView *)collectionView {
    NSLog(@"%s", __FUNCTION__);
    return [[self dailyForecasts] count];
}

- (UIView *)collectionView:(PSCollectionView *)collectionView cellForRowAtIndex:(NSInteger)index {
    NSLog(@"%s index: %d", __FUNCTION__, index);
    PinnyCollectionCell *cell = (PinnyCollectionCell *)[self.collectionView dequeueReusableViewForClass:[PinnyCollectionCell class]];
    if (cell == nil) {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"PinnyCollectionCell" owner:self options:nil];
        cell = (PinnyCollectionCell *)[nibs objectAtIndex:0];
    }
    
    NSDictionary* dailyForecast = [[self dailyForecasts] objectAtIndex:index];
    [cell setForecast:dailyForecast];
    return cell;
}

- (CGFloat)collectionView:(PSCollectionView *)collectionView heightForRowAtIndex:(NSInteger)index {
    
    NSDictionary* dailyForecast = [[self dailyForecasts] objectAtIndex:index];
    return [[dailyForecast allKeys] count] * 44 + 20;
}

@end
