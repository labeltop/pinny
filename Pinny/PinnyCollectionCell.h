//
//  PinnyCollectionCell.h
//  Pinny
//
//  Created by hasan on 6/3/13.
//  Copyright (c) 2013 Hasan Adil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSCollectionViewCell.h"

@interface PinnyCollectionCell : PSCollectionViewCell

@property (nonatomic, strong) IBOutlet UITableView* table;

-(void) setForecast:(NSDictionary*)d;

@end
