//
//  PinnyCollectionCell.m
//  Pinny
//
//  Created by hasan on 6/3/13.
//  Copyright (c) 2013 Hasan Adil. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "PinnyCollectionCell.h"

@interface PinnyCollectionCell() <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSDictionary* forecastDictionary;
@property (nonatomic, strong) NSArray* keys;

@end

@implementation PinnyCollectionCell

@synthesize forecastDictionary, keys, table;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    return self;
}

-(void) setForecast:(NSDictionary*)d
{
    NSLog(@"%s", __FUNCTION__);
    [self setForecastDictionary:d];
    [self setKeys:[self.forecastDictionary allKeys]];
    [[self table] reloadData];
    [[[self table] layer] setCornerRadius:5];
    [[self table] setScrollEnabled:NO];
}

#pragma mark table view datasource

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSNumber* time = [[self forecastDictionary] objectForKey:@"time"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[time longValue]];
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterLongStyle];
    return [formatter stringFromDate:date];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self keys] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cell"];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    NSString* key = [[self keys] objectAtIndex:indexPath.row];
    NSString* value = [[self forecastDictionary] objectForKey:key];
    [[cell textLabel] setText:key];
    [[cell detailTextLabel] setText:[NSString stringWithFormat:@"%@", value]];
    return cell;
}

@end





